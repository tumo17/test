<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE HTML>
<html lang="ja">

<!--// HEAD //////////////////////////////////////////////////////////-->
<head>
<title>Practice00</title>
<meta charset="UTF-8">
<style>
<!--
	body         { text-align:center; }
	div#div_main { height:400px; }
-->
</style>
</head>

<!--// BODY //////////////////////////////////////////////////////////-->
<body>

<div id="div_head">
	<h1>Practice00</h1>
</div>
<hr>

<div id="div_main">

</div>
<hr>

<div id="div_foot">
	<p>&copy;2014 shibata_ts All Rights Reserved.</p>
</div>

<!--// SCRIPT ////////////////////////////////////////////////////////-->
<script type="text/javascript">
<!--

-->
</script>
<!--//////////////////////////////////////////////////////////////////-->
</body>
</html>



